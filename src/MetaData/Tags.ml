
let copy tag =
  let new_tag = Common.Tag.fresh () in
  Position.copy tag new_tag;
  new_tag
