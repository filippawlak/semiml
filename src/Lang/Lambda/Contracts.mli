
(** Contract [right_scopes] states that every variable is bound in the program.
*)
val right_scopes : Contract.t

(** Contract [unique_vars] states that there are no two different local 
variables represeted by the same value (of type [Common.var]). *)
val unique_vars  : Contract.t

(** Contract [unique_tags] states that every tag in the program is unique. *)
val unique_tags  : Contract.t

(** Contract [switch_well_formed] states that every switch expression
well-formed. It means that:
1. it is exhaustive, i.e., each case declared by [sw_possible_cons] and 
   [sw_constant_range] is covered by some clause
2. if there is a clause for [C_Transparent] constructor, then this is the only
   clause in switch expression 
3. if there is a clause for [C_TransU], then there are no constant clauses 
4. if there is a clause for [C_TransB], then this is the only constructor
   clause in switch expression *)
val switch_well_formed : Contract.t
