open Language

let right_scopes = Common.Contracts.right_scopes
let unique_vars  = Common.Contracts.unique_vars
let unique_tags  = Common.Contracts.unique_tags

let primop_arity = Contract.create
  ~description:
    "Every primop operation has right number of arguments, \
    binds right number of variables and has right number of continuations."
  ~languages: [CPS]
  "contract:primop_arity"
