
type t =
| Arith   of Common.Primop.Arith.t
| FArith  of Common.Primop.FArith.t
| Mem     of Common.Primop.Mem.t
| Repr    of Common.Primop.Repr.t
| Exn     of Common.Primop.Exn.t

let arity op =
  match op with
  | Arith  op -> Common.Primop.Arith.arity  op
  | FArith op -> Common.Primop.FArith.arity op
  | Mem    op -> Common.Primop.Mem.arity    op
  | Repr   op -> Common.Primop.Repr.arity   op
  | Exn    op -> Common.Primop.Exn.arity    op

let cps_value_n op =
  match op with
  | Arith  op -> Common.Primop.Arith.cps_value_n  op
  | FArith op -> Common.Primop.FArith.cps_value_n op
  | Mem    op -> Common.Primop.Mem.cps_value_n    op
  | Repr   op -> Common.Primop.Repr.cps_value_n   op
  | Exn    op -> Common.Primop.Exn.cps_value_n    op

let cps_cont_n op =
  match op with
  | Arith  op -> Common.Primop.Arith.cps_cont_n  op
  | FArith op -> Common.Primop.FArith.cps_cont_n op
  | Mem    op -> Common.Primop.Mem.cps_cont_n    op
  | Repr   op -> Common.Primop.Repr.cps_cont_n   op
  | Exn    op -> Common.Primop.Exn.cps_cont_n    op
