
type env =
  { tvar_env : t Type.TVar.Map.t
  ; tcon_env : Type.TCon.t Type.TCon.Map.t
  }

and t = env * Type.t

let empty_env =
  { tvar_env = Type.TVar.Map.empty
  ; tcon_env = Type.TCon.Map.empty
  }

let of_type tp = (empty_env, tp)

type typedef = env * (Type.tvar list * Type.t)
type datatype_def = env * (Type.tvar list * (Type.cname * Type.t option) list)
type local_datatype = 
  env * (Type.datatype_def list * Type.datatype_def list * Type.t)

let datatype_def args ctors = (empty_env, (args, ctors))

type _ tvar_binder =
| TVB : Type.tvar * env * 'a -> (env * 'a) tvar_binder

type _ tcon_binder =
| TCB : int * Type.tcon * env * 'a -> (env * 'a) tcon_binder

type typedef_view =
| TD_Abs  of typedef tvar_binder
| TD_Body of t

type datatype_view =
| DT_Abs   of datatype_def tvar_binder
| DT_Ctors of (Type.cname * t option) list

type local_datatype_view =
| LDT_Abs      of local_datatype tcon_binder
| LDT_DataType of (Type.tcon * datatype_def) list * t

type view =
| Var       of Type.tvar
| TyCon     of t list * Type.tcon
| Arrow     of t * t
| Record    of t list
| TypeDef   of typedef * t tcon_binder
| DataType  of local_datatype
| ForallVar of t tvar_binder
| Forall    of t tcon_binder
| Exists    of t tcon_binder

let open_fresh_tvar (type a) y (bind : a tvar_binder) : a =
  match bind with
  | TVB(x, env, tp) ->
    let env =
      { env with
        tvar_env = Type.TVar.Map.add x (of_type (Type.Var y)) env.tvar_env
      } in
    (env, tp)

let open_tvar (type a) (bind : a tvar_binder) : Type.tvar * a =
  match bind with
  | TVB(x, _, _) ->
    let y = Type.TVar.copy x in
    (y, open_fresh_tvar y bind)

let close_tvar x (env, a) = TVB(x, env, a)

let open_fresh_tcon (type a) y (bind : a tcon_binder) : int * a =
  match bind with
  | TCB(n, x, env, tp) ->
    let env =
      { env with
        tcon_env = Type.TCon.Map.add x y env.tcon_env
      } in
    (n, (env, tp))

let open_tcon (type a) (bind : a tcon_binder) : int * Type.tcon * a =
  match bind with
  | TCB(n, x, _, _) ->
    let y = Type.TCon.copy x in
    (n, y, snd (open_fresh_tcon y bind))

let close_tcon n x (env, tp) = TCB(n, x, env, tp)

let shift_env env tp = (env, tp)

let view_typedef (env, (args, body)) =
  match args with
  | [] -> TD_Body(env, body)
  | x :: xs -> TD_Abs(TVB(x, env, (xs, body)))

let view_datatype (env, (args, ctors)) =
  match args with
  | [] -> DT_Ctors (List.map 
      (fun (c, tp) ->
        match tp with
        | None -> (c, None)
        | Some tp -> (c, Some (env, tp))
      ) ctors)
  | x :: xs -> DT_Abs(TVB(x, env, (xs, ctors)))

let view_tcon env x =
  try Type.TCon.Map.find x env.tcon_env with
  | Not_found -> x

let view_local_datatype (env, (defs, defs0, tp)) =
  match defs with
  | [] -> LDT_DataType(
      List.map (fun def ->
          ( view_tcon env def.Type.dt_name
          ,(env, (def.Type.dt_args, def.Type.dt_ctors)))
        ) defs0,
        (env, tp))
  | def :: defs ->
    LDT_Abs(TCB(
      List.length def.Type.dt_args,
      def.Type.dt_name,
      env,
      (defs, defs0, tp)))

let rec view_tvar env x =
  match Type.TVar.Map.find x env.tvar_env with
  | tp -> view tp
  | exception Not_found -> Var x

and view (env, tp) =
  match tp with
  | Type.Var x          -> view_tvar env x
  | Type.TyCon(args, c) -> 
    TyCon(List.map (shift_env env) args, view_tcon env c)
  | Type.Arrow(tp1, tp2) ->
    Arrow(shift_env env tp1, shift_env env tp2)
  | Type.Record tps ->
    Record(List.map (shift_env env) tps)
  | Type.TypeDef(args, c, body, tp2) ->
    TypeDef((env, (args, body)),
      TCB(List.length args, c, env, tp2))
  | Type.DataType(defs, tp) ->
    DataType(env, (defs, defs, tp))
  | Type.ForallVar(x, body) -> ForallVar(TVB(x, env, body))
  | Type.Forall(n, x, body) -> Forall(TCB(n, x, env, body))
  | Type.Exists(n, x, body) -> Exists(TCB(n, x, env, body))

let rec to_type tp =
  match view tp with
  | Var x           -> Type.Var x
  | TyCon(args, c)  -> Type.TyCon(List.map to_type args, c)
  | Arrow(tp1, tp2) -> Type.Arrow(to_type tp1, to_type tp2)
  | Record tps      -> Type.Record(List.map to_type tps)
  | TypeDef(def, bind) ->
    let (args, body) = concrete_typedef def in
    let (_, x, tp) = open_tcon bind in
    Type.TypeDef(args, x, body, to_type tp)
  | DataType ldt -> concrete_local_datatype ldt
  | ForallVar bind  ->
    let (x, tp) = open_tvar bind in
    Type.ForallVar(x, to_type tp)
  | Forall bind ->
    let (n, x, tp) = open_tcon bind in
    Type.Forall(n, x, to_type tp)
  | Exists bind ->
    let (n, x, tp) = open_tcon bind in
    Type.Exists(n, x, to_type tp)

and concrete_typedef def =
  match view_typedef def with
  | TD_Abs bind ->
    let (x, def) = open_tvar bind in
    let (xs, body) = concrete_typedef def in
    (x :: xs, body)
  | TD_Body tp -> ([], to_type tp)

and concrete_datatype_def def =
  match view_datatype def with
  | DT_Abs bind ->
    let (x, def) = open_tvar bind in
    let (xs, ctors) = concrete_datatype_def def in
    (x :: xs, ctors)
  | DT_Ctors ctors ->
    ([], List.map
      (fun (c, tp) ->
        match tp with
        | None    -> (c, None)
        | Some tp -> (c, Some (to_type tp))
      ) ctors)

and concrete_datatype (x, def) =
  let (args, ctors) = concrete_datatype_def def in
  { Type.dt_args  = args
  ; Type.dt_name  = x
  ; Type.dt_ctors = ctors
  }

and concrete_local_datatype ldt =
  match view_local_datatype ldt with
  | LDT_Abs bind ->
    let (_, _, ldt) = open_tcon bind in
    concrete_local_datatype ldt
  | LDT_DataType(defs, tp) ->
    Type.DataType(List.map concrete_datatype defs, to_type tp)

let rec subst x s (env, tp) =
  { env with tvar_env = 
    Type.TVar.Map.add x s (Type.TVar.Map.map (subst x s) env.tvar_env)
  }, tp

let rec unfold_typedef args def =
  match args, view_typedef def with
  | [], TD_Body tp -> tp
  | arg :: args, TD_Abs bind ->
    let (x, def) = open_tvar bind in
    subst x arg (unfold_typedef args def)
  | _ -> failwith "unfold_typedef"

let define_aux x def tp =
  let tp = to_type tp in
  if Type.contains_tcon x tp then begin
    let (args, body) = concrete_typedef def in
    Type.TypeDef(args, x, body, tp)
  end else tp

let define x def tp =
  of_type (define_aux x def tp)

let rec define_in_datatype_def_aux x def ddef =
  match view_datatype ddef with
  | DT_Abs bind ->
    let (y, ddef) = open_tvar bind in
    let (args, ctors) = define_in_datatype_def_aux x def ddef in
    (y :: args, ctors)
  | DT_Ctors ctors ->
    ([], List.map 
      (fun (z, ctor) ->
        match ctor with
        | None   -> (z, None)
        | Some t -> (z, Some (define_aux x def t))
      ) ctors)

let define_in_datatype_def x def ddef =
  (empty_env, define_in_datatype_def_aux x def ddef)

let datatype_aux defs tp =
  let tp = to_type tp in
  if List.exists (fun (x, _) -> Type.contains_tcon x tp) defs then begin
    let defs = List.map concrete_datatype defs in
    Type.DataType(defs, tp)
  end else tp

let datatype defs tp =
  of_type (datatype_aux defs tp)

let rec datatype_in_datatype_def_aux defs def =
  match view_datatype def with
  | DT_Abs bind ->
    let (y, def) = open_tvar bind in
    let (args, ctors) = datatype_in_datatype_def_aux defs def in
    (y :: args, ctors)
  | DT_Ctors ctors ->
    ([], List.map 
      (fun (z, ctor) ->
        match ctor with
        | None   -> (z, None)
        | Some t -> (z, Some (datatype_aux defs t))
      ) ctors)

let datatype_in_datatype_def defs def =
  (empty_env, (datatype_in_datatype_def_aux defs def))
