
{

exception Invalid_character of char

let kw_map = Hashtbl.create 32
let _ =
  Hashtbl.add kw_map "and"    YaccParser.KW_AND;
  Hashtbl.add kw_map "as"     YaccParser.KW_AS;
  Hashtbl.add kw_map "call"   YaccParser.KW_CALL;
  Hashtbl.add kw_map "end"    YaccParser.KW_END;
  Hashtbl.add kw_map "fix"    YaccParser.KW_FIX;
  Hashtbl.add kw_map "label"  YaccParser.KW_LABEL;
  Hashtbl.add kw_map "let"    YaccParser.KW_LET;
  Hashtbl.add kw_map "offset" YaccParser.KW_OFFSET;
  Hashtbl.add kw_map "primop" YaccParser.KW_PRIMOP;
  Hashtbl.add kw_map "record" YaccParser.KW_RECORD;
  Hashtbl.add kw_map "switch" YaccParser.KW_SWITCH;
  ()

let op_map = Hashtbl.create 32
let _ =
  Hashtbl.add op_map "=>" YaccParser.ARROW2;
  Hashtbl.add op_map "="  YaccParser.EQ;
  Hashtbl.add op_map "#"  YaccParser.HASH;
  ()

let tokenize_ident str =
  try Hashtbl.find kw_map str with
  | Not_found -> YaccParser.ID str

let tokenize_oper str =
  try Hashtbl.find op_map str with
  | Not_found -> YaccParser.ID str

let tokenize_int str lexbuf =
  let str =
    if str.[0] = '~' then
      "-" ^ String.sub str 1 (String.length str - 1)
    else
      str
  in
  try YaccParser.INT (Int64.of_string str) with
  | Failure _ ->
    Errors.error_pp
      lexbuf.Lexing.lex_start_p
      lexbuf.Lexing.lex_curr_p
      "Too large integer constant";
    raise Errors.Fatal_error

let tokenize_real str =
  YaccParser.REAL (Common.Real.of_sml_string str)

}

let digit     = ['0'-'9']
let var_start = ['a'-'z' 'A'-'Z']
let var_char  = var_start | digit | '\'' | '_'

let op_char = 
  ['!' '%' '&' '$' '#' '+' '-' '/' ':' '<' '=' '>' '?' 
   '@' '\\' '~' '`' '^' '|' '*' ]

let hex_digit = ['0'-'9' 'a'-'f' 'A'-'F']
let str_printable = '!' | ['#'-'[' ']'-'~']
let whitespace = ['\011'-'\r' '\t' ' ']

rule token = parse
    whitespace+ { token lexbuf }
  | '\n' { Lexing.new_line lexbuf; token lexbuf }
  | "(*" { block_comment 1 lexbuf }
  | '(' { YaccParser.BR_OPN  }
  | ')' { YaccParser.BR_CLS  }
  | ',' { YaccParser.COMMA   }
  | var_start var_char* as x { tokenize_ident x }
  | op_char* as x { tokenize_oper x }
  | '~'? digit+ as x { tokenize_int x lexbuf }
  | '~'? "0x" hex_digit+ as x { tokenize_int x lexbuf }
  | '~'? digit+ ('.' digit+)? ['e' 'E'] '~'? digit+ as x { tokenize_real x }
  | '~'? digit+ '.' digit+ as x { tokenize_real x }
  | '"' { 
      let buf = Buffer.create 32 in
      string_constant lexbuf.Lexing.lex_start_p buf lexbuf 
    }
  | eof { YaccParser.EOF }
  | _ as x { raise (Invalid_character x) }

and string_constant start_pos buf = parse
    (str_printable | ' ')* as x {
        Buffer.add_string buf x; string_constant start_pos buf lexbuf
      }
  | "\\a" { Buffer.add_char buf '\007'; string_constant start_pos buf lexbuf }
  | "\\b" { Buffer.add_char buf '\008'; string_constant start_pos buf lexbuf }
  | "\\t" { Buffer.add_char buf '\009'; string_constant start_pos buf lexbuf }
  | "\\n" { Buffer.add_char buf '\010'; string_constant start_pos buf lexbuf }
  | "\\v" { Buffer.add_char buf '\011'; string_constant start_pos buf lexbuf }
  | "\\f" { Buffer.add_char buf '\012'; string_constant start_pos buf lexbuf }
  | "\\r" { Buffer.add_char buf '\013'; string_constant start_pos buf lexbuf }
  | "\\^" (['@'-'_'] as x) { 
      Buffer.add_char buf (Char.chr (Char.code x - 64)); 
      string_constant start_pos buf lexbuf 
    }
  | "\\" (digit digit digit as x) {
      let n = int_of_string x in
      if n >= 256 then begin
        Errors.error_pp
          lexbuf.Lexing.lex_start_p
          lexbuf.Lexing.lex_curr_p
          "Illegal escape sequence.";
        raise Errors.Fatal_error
      end;
      Buffer.add_char buf (Char.chr n);
      string_constant start_pos buf lexbuf
    }
  | "\\\"" { Buffer.add_char buf '"';  string_constant start_pos buf lexbuf }
  | "\\\\" { Buffer.add_char buf '\\'; string_constant start_pos buf lexbuf }
  | "\"" {
      lexbuf.Lexing.lex_start_p <- start_pos;
      YaccParser.STRING (Buffer.contents buf)
    }
  | "\\" whitespace { string_skip_ws start_pos buf lexbuf }
  | "\\\n" { Lexing.new_line lexbuf; string_skip_ws start_pos buf lexbuf }
  | "\\" (digit? digit? digit? | str_printable?) {
      Errors.error_pp
        lexbuf.Lexing.lex_start_p
        lexbuf.Lexing.lex_curr_p
        "Illegal escape sequence.";
      raise Errors.Fatal_error
    }
  | ("\n" | eof) {
      Errors.error_lp lexbuf.Lexing.lex_curr_p
        "Unclosed string";
      raise Errors.Fatal_error
    }
  | _ as x { raise (Invalid_character x) }

and string_skip_ws start_pos buf = parse
    '\n' { Lexing.new_line lexbuf; string_skip_ws start_pos buf lexbuf }
  | whitespace+ { string_skip_ws start_pos buf lexbuf }
  | "\\" { string_constant start_pos buf lexbuf }
  | _ {
      Errors.error_lp lexbuf.Lexing.lex_curr_p
        "Unclosed string";
      raise Errors.Fatal_error
    }

and block_comment depth = parse
    '\n' { Lexing.new_line lexbuf; block_comment depth lexbuf }
  | "(*" { block_comment (depth+1) lexbuf }
  | "*)" {
      if depth = 1 then token lexbuf
      else block_comment (depth-1) lexbuf
    }
  | eof  {
      Errors.error_lp lexbuf.Lexing.lex_curr_p
        "End of file inside block comment. `*)' was expected.";
      raise Errors.Fatal_error
    }
  | _ { block_comment depth lexbuf }
