open Lang.CPS.Ast

let rec check_expr expr =
  match expr.e_kind with
  | Record(_, _, e) | Select(_, _, _, e) | Offset(_, _, _, e) ->
    check_expr e
  | App _ -> true
  | Fix(defs, e) ->
    List.for_all (fun (_, _, body) -> check_expr body) defs
    && check_expr e
  | Switch(_, es) -> List.for_all check_expr es
  | Primop(op, vs, xs, es) ->
    Lang.CPS.Primop.arity       op = List.length vs &&
    Lang.CPS.Primop.cps_value_n op = List.length xs &&
    Lang.CPS.Primop.cps_cont_n  op = List.length es &&
    List.for_all check_expr es

let check_program expr =
  check_expr expr

let register () =
  Compiler.register_contract_checker
    ~lang: Compiler.Lang_CPS
    ~name: "CPS:primop_arity"
    ~contract: Lang.CPS.Contracts.primop_arity
    check_program
