
module Contr = struct
  type t =
    { name        : string
    ; id          : int64
    ; description : string option
    ; languages   : Language.t list option
    }

  let compare c1 c2 = Int64.compare c1.id c2.id
end

include Contr

let all_contracts = Queue.create ()

let equal c1 c2 = c1 == c2

let next_fresh_id = ref 0L
let fresh_id () =
  let r = !next_fresh_id in
  next_fresh_id := Int64.succ r;
  r

let create ?description ?languages name =
  let c =
    { id          = fresh_id ()
    ; name        = name
    ; languages   = languages
    ; description = description
    }
  in
  Queue.add c all_contracts;
  c

let name c = c.name
let description c = c.description
let languages c = c.languages

let pretty c =
  let module Box = Printing.Box in
  Box.box (
    Box.br (Box.text c.name) ::
    begin match c.description with
    | None -> []
    | Some descr -> [ Box.indent 2 (Box.text descr) ]
    end @
    begin match c.languages with
    | None -> []
    | Some langs -> 
      [ Box.indent 2 (Box.box
        [ Box.text "Avaiable for language(s):"
        ; Box.indent 2 (Box.box (List.map (fun lang ->
            Box.white_sep (Box.text (Language.name lang))
          ) langs))
        ])
      ]
    end)

let iter f = Queue.iter f all_contracts

type action =
| Add    of t list
| Set    of t list
| Remove of t list

type rule = t list * action

let saves_contract c = ([c], Add [c])

module Set = Set.Make(Contr)

let apply_rule contracts (cnd, action) new_contracts =
  if List.for_all (fun c -> Set.mem c contracts) cnd then
    match action with
    | Add cs    -> Set.union new_contracts (Set.of_list cs)
    | Set cs    -> Set.of_list cs
    | Remove cs -> Set.diff new_contracts (Set.of_list cs)
  else new_contracts

let apply_rules rules contracts =
  List.fold_left (fun new_contracts rule ->
    apply_rule contracts rule new_contracts
  ) contracts rules
