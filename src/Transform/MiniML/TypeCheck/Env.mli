
type t

val empty : t

val tcon_arity : t -> Lang.MiniML.Type.tcon -> int
val type_equal : t -> Lang.MiniML.Type.t -> Lang.MiniML.Type.t -> bool

val try_type_as_arrow :
  t ->
  Lang.MiniML.Type.t ->
    (Lang.MiniML.Type.t * Lang.MiniML.Type.t) option
val try_type_as_record : 
  t -> Lang.MiniML.Type.t -> Lang.MiniML.Type.t list option
val try_type_as_forall_var :
  t ->
  Lang.MiniML.Type.t ->
    (Lang.MiniML.TypeView.t Lang.MiniML.TypeView.tvar_binder) option
val try_type_as_forall :
  t ->
  Lang.MiniML.Type.t ->
    (Lang.MiniML.TypeView.t Lang.MiniML.TypeView.tcon_binder) option
val try_type_as_exists :
  t ->
  Lang.MiniML.Type.t ->
    (Lang.MiniML.TypeView.t Lang.MiniML.TypeView.tcon_binder) option

type var =
| Var       of Common.Var.t * Lang.MiniML.Type.t
| ConstCtor of Lang.MiniML.Type.cname * int * Lang.MiniML.Type.tcon
| DataCtor  of Lang.MiniML.Type.cname *
    Lang.MiniML.Type.tvar list * Lang.MiniML.Type.tcon * Lang.MiniML.Type.t

val add_var : t -> string -> Lang.MiniML.Type.t -> t * Common.Var.t
val add_const_ctor : 
  t -> string -> int -> Lang.MiniML.Type.tcon -> t * Lang.MiniML.Type.cname
val add_data_ctor :
  t ->
  string ->
  Lang.MiniML.Type.tvar list ->
  Lang.MiniML.Type.tcon ->
  Lang.MiniML.Type.t ->
    t * Lang.MiniML.Type.cname
val get_var : t -> string -> var

val add_tvar : t -> string -> t * Lang.MiniML.Type.tvar
val get_tvar : t -> string -> Lang.MiniML.Type.tvar

val add_abstype : t -> string -> int -> t * Lang.MiniML.Type.tcon
val add_typedef :
  t ->
  string ->
  Lang.MiniML.Type.tvar list ->
  Lang.MiniML.Type.t ->
    t * Lang.MiniML.Type.tcon
val add_datatype :
  t ->
  Lang.MiniML.Type.tvar list ->
  Lang.MiniML.Type.tcon ->
  (Lang.MiniML.Type.cname * Lang.MiniML.Type.t option) list ->
    t
val get_type    : t -> string -> Lang.MiniML.Type.tcon

val get_datatype : t -> Lang.MiniML.Type.tcon -> Lang.MiniML.Type.datatype_def
val ctors_of_datatype :
  t -> Lang.MiniML.Type.tcon -> Lang.MiniML.Type.CName.Set.t

val type_env   : t -> Lang.MiniML.TypeEnv.t
val type_scope : t -> Lang.MiniML.TypeEnv.scope
