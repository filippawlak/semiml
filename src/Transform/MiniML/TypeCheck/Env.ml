
module StrMap  = Map.Make(String)
module TypeEnv = Lang.MiniML.TypeEnv

type var =
| Var       of Common.Var.t * Lang.MiniML.Type.t
| ConstCtor of Lang.MiniML.Type.cname * int * Lang.MiniML.Type.tcon
| DataCtor  of Lang.MiniML.Type.cname *
    Lang.MiniML.Type.tvar list * Lang.MiniML.Type.tcon * Lang.MiniML.Type.t

type t =
  { var_env  : var StrMap.t
  ; tvar_env : Lang.MiniML.Type.tvar StrMap.t
  ; tcon_env : Lang.MiniML.Type.tcon StrMap.t
  ; type_env : TypeEnv.t
  }

let tcon_arity env x =
  Lang.MiniML.TypeEnv.arity env.type_env x

let type_equal env tp1 tp2 =
  Lang.MiniML.TypeEq.equal env.type_env tp1 tp2

let try_type_as_arrow env tp =
  Lang.MiniML.TypeNF.try_as_arrow env.type_env tp

let try_type_as_record env tp =
  Lang.MiniML.TypeNF.try_as_record env.type_env tp

let try_type_as_forall_var env tp =
  Lang.MiniML.TypeNF.try_as_forall_var env.type_env tp

let try_type_as_forall env tp =
  Lang.MiniML.TypeNF.try_as_forall env.type_env tp

let try_type_as_exists env tp =
  Lang.MiniML.TypeNF.try_as_exists env.type_env tp

let rec map_of_list acc add xs =
  match xs with
  | [] -> acc
  | (x, v) :: xs -> map_of_list (add x v acc) add xs

let empty =
  { var_env  = StrMap.empty
  ; tvar_env = StrMap.empty
  ; tcon_env =
    map_of_list StrMap.empty StrMap.add
      (List.map 
        (fun (x, _) -> (Lang.MiniML.Type.TCon.name x, x))
        Lang.MiniML.Type.predefined_types)
  ; type_env = TypeEnv.empty
  }

let add_const_ctor env x n c =
  let y = Lang.MiniML.Type.CName.create ~name:x () in
  { env with
    var_env = StrMap.add x (ConstCtor(y, n, c)) env.var_env
  }, y

let add_data_ctor env x args c tp =
  let y = Lang.MiniML.Type.CName.create ~name:x () in
  { env with
    var_env = StrMap.add x (DataCtor(y, args, c, tp)) env.var_env
  }, y

let add_var env x tp =
  let y = Common.Var.create ~name:x () in
  { env with
    var_env = StrMap.add x (Var(y, tp)) env.var_env
  }, y

let get_var env x =
  StrMap.find x env.var_env

let add_tvar env x =
  let y = Lang.MiniML.Type.TVar.create ~name:x () in
  let (type_env, y) = TypeEnv.add_tvar env.type_env y in
  { env with
    tvar_env = StrMap.add x y env.tvar_env
  ; type_env = type_env
  }, y

let add_tcon env x typedef =
  let y = Lang.MiniML.Type.TCon.create ~name:x () in
  let (type_env, y) = TypeEnv.add_tcon env.type_env y typedef in
  { env with
    tcon_env = StrMap.add x y env.tcon_env
  ; type_env = type_env
  }, y

let add_abstype env x n =
  add_tcon env x (TypeEnv.Abstract n)

let add_typedef env x args body =
  add_tcon env x (TypeEnv.TypeDef(args, body))

let add_datatype env args x ctors =
  { env with
    type_env = TypeEnv.upgrade_to_datatype env.type_env args x ctors
  }

let get_tvar env x =
  StrMap.find x env.tvar_env

let get_type env x =
  StrMap.find x env.tcon_env

let get_datatype env x =
  match TypeEnv.get_tcon env.type_env x with
  | TypeEnv.DataType(args, ctors) ->
    { Lang.MiniML.Type.dt_args  = args
    ; Lang.MiniML.Type.dt_name  = x
    ; Lang.MiniML.Type.dt_ctors = ctors
    }
  | _ -> assert false

let ctors_of_datatype env x =
  let dt = get_datatype env x in
  List.fold_left (fun s (c, _) ->
    Lang.MiniML.Type.CName.Set.add c s
  ) Lang.MiniML.Type.CName.Set.empty dt.Lang.MiniML.Type.dt_ctors

let type_env env = env.type_env
let type_scope env = TypeEnv.scope env.type_env
