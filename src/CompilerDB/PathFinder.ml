
module NodeSet = Set.Make(GraphTypes.Node)

let rec finder_loop known_nodes queue is_dest =
  if Queue.is_empty queue then
    raise Not_found
  else
  let state = Queue.pop queue in
  match state with
  | (GraphTypes.Node(lang, contracts), rev_path) ->
    let rec add_transformations known_nodes transformations =
      match transformations with
      | [] -> 
        add_analyses 
          known_nodes 
          (AnalysisDB.possible_analyses lang contracts)
      | trans :: transformations ->
        let new_path = GraphTypes.Cmd_Transform trans :: rev_path in
        let (Transform.Transform(_, tgt_lang, trans_data)) = trans in
        let new_contracts =
          Contract.apply_rules trans_data.Transform.contract_rules contracts in
        let new_node = GraphTypes.Node(tgt_lang, new_contracts) in
        begin match is_dest new_node with
        | Some result -> (new_path, result)
        | None -> 
          if NodeSet.mem new_node known_nodes then
            add_transformations known_nodes transformations
          else begin
            let known_nodes = NodeSet.add new_node known_nodes in
            Queue.push (new_node, new_path) queue;
            add_transformations known_nodes transformations
          end
        end
    and add_analyses known_nodes analyses =
      match analyses with
      | [] -> finder_loop known_nodes queue is_dest
      | analysis :: analyses ->
        let new_path = GraphTypes.Cmd_Analyse analysis :: rev_path in
        let (Analysis.Analysis(_, adata)) = analysis in
        let new_contracts =
          Contract.apply_rules adata.Analysis.contract_rules contracts in
        let new_node = GraphTypes.Node(lang, new_contracts) in
        begin match is_dest new_node with
        | Some result -> (rev_path, result)
        | None ->
          if NodeSet.mem new_node known_nodes then
            add_analyses known_nodes analyses
          else begin
            let known_nodes = NodeSet.add new_node known_nodes in
            Queue.push (new_node, new_path) queue;
            add_analyses known_nodes analyses
          end
        end
    in
    add_transformations
      known_nodes
      (TransformDB.possible_transforms lang contracts)

let find_path source is_dest last_cmds =
  match is_dest source with
  | Some result -> last_cmds result
  | None ->
    let known_nodes = NodeSet.empty in
    let queue = Queue.create () in
    Queue.push (source, []) queue;
    let (rev_path, result) = finder_loop known_nodes queue is_dest in
    List.rev_append rev_path (last_cmds result)
